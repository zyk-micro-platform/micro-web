const managerPage = {
  data() {
    return {
      tableHeight: '400',
      showTable: false
    }
  },
  mounted() {
    const pageTableView = this.$refs['page-table-view']
    if (pageTableView) {
      this.tableHeight = pageTableView.clientHeight
      this.showTable = true
    }
  }
}

export default managerPage
