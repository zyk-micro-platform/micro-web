import defaultSettings from '@/settings'

const title = defaultSettings.title || 'SYSTEM-MANAGER-WEB'

export default function getPageTitle(pageTitle) {
  if (pageTitle) {
    return `${pageTitle} - ${title}`
  }
  return `${title}`
}
