import axios from 'axios'
import { MessageBox, Message } from 'element-ui'
import store from '@/store'
import { getToken } from '@/utils/auth'
import { encrypt, decrypt } from '@/utils/rsa'

const publicPaths = ['/oauth/captcha/graph', '/oauth/captcha/graph/print', '/oauth/captcha/graph', '/oauth/captcha/sms']

const encryptHandler = function(config) {
  if (publicPaths.includes(config.url)) {
    return config
  }
  if (config.data) {
    config.data = { encryptData: encrypt(JSON.stringify(config.data)) }
  }
  return config
}

const decryptHandler = function(response) {
  if (publicPaths.includes(response.config.url.replace(response.config.baseURL, ``))) {
    return response.data
  }
  if (response.data) {
    const json = decrypt(response.data)
    console.log(json)
    return JSON.parse(json || `{}`)
  }
  return response.data
}

const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API,
  timeout: 50000
})

service.interceptors.request.use(
  config => {
    if (store.getters.token) {
      config.headers['access-token'] = getToken()
    }
    config = encryptHandler(config)
    return config
  },
  error => {
    console.log(error)
    return Promise.reject(error)
  }
)

service.interceptors.response.use(
  response => {
    const res = decryptHandler(response)
    if (res.code !== 200) {
      Message({
        message: res.message || 'Error',
        type: 'error',
        duration: 5 * 1000
      })
      if (res.code === 50008 || res.code === 50012 || res.code === 50014) {
        MessageBox.confirm('You have been logged out, you can cancel to stay on this page, or log in again', 'Confirm logout', {
          confirmButtonText: 'Re-Login',
          cancelButtonText: 'Cancel',
          type: 'warning'
        }).then(() => {
          store.dispatch('user/resetToken').then(() => {
            location.reload()
          })
        })
      }
      return Promise.reject(new Error(res.message || 'Error'))
    } else {
      return res
    }
  },
  error => {
    console.log('err' + error)
    Message({
      message: error.message,
      type: 'error',
      duration: 5 * 1000
    })
    return Promise.reject(error)
  }
)

export default service
