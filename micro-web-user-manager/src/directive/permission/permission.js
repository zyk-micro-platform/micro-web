
function checkPermission(el, binding, vnode) {
  const { value } = binding
  const apis = vnode.context.$route.meta.apis
  if (value && value instanceof Array) {
    if (value.length > 0) {
      const permissionApis = value
      const hasPermission = permissionApis.every(role => {
        return apis.includes(role)
      })
      if (!hasPermission) {
        el.parentNode && el.parentNode.removeChild(el)
      }
    }
  } else {
    throw new Error(`need api permissions`)
  }
}

export default {
  inserted(el, binding, vnode) {
    checkPermission(el, binding, vnode)
  }
}
