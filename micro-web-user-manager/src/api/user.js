import request from '@/utils/request'

export function newUser(data) {
  return request({
    url: '/user/user/new',
    method: 'post',
    data
  })
}

export function pageFilter(data) {
  return request({
    url: '/user/user/pageFilter',
    method: 'post',
    data
  })
}

export function edited(data) {
  return request({
    url: '/user/user/edited',
    method: 'post',
    data
  })
}

export function deleted(data) {
  return request({
    url: '/user/user/deleted',
    method: 'post',
    data
  })
}
