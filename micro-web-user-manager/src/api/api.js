import request from '@/utils/request'

export function apiTogether() {
  return request({
    url: '/oauth/apis/together-list',
    method: 'post'
  })
}
