import request from '@/utils/request'

export function list() {
  return request({
    url: '/user/dept/list',
    method: 'post'
  })
}

export function newsDept(data) {
  return request({
    url: '/user/dept/new',
    method: 'post',
    data
  })
}

export function edited(data) {
  return request({
    url: '/user/dept/edited',
    method: 'post',
    data
  })
}

export function deleted(data) {
  return request({
    url: '/user/dept/deleted',
    method: 'post',
    data
  })
}
