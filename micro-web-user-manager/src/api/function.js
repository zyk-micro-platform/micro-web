import request from '@/utils/request'

export function list() {
  return request({
    url: '/user/function/list',
    method: 'post'
  })
}

export function listByMenuId(params) {
  return request({
    url: '/user/function/listByMenuId',
    method: 'post',
    params
  })
}

export function newsFunction(data) {
  return request({
    url: '/user/function/new',
    method: 'post',
    data
  })
}

export function edited(data) {
  return request({
    url: '/user/function/edited',
    method: 'post',
    data
  })
}

export function deleted(data) {
  return request({
    url: '/user/function/deleted',
    method: 'post',
    data
  })
}
