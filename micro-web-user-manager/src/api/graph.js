import request from '@/utils/request'
export function graph() {
  return request({
    url: '/oauth/captcha/graph',
    method: 'get'
  })
}
