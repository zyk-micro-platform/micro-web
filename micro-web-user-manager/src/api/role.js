import request from '@/utils/request'

export function newRole(data) {
  return request({
    url: '/user/role/new',
    method: 'post',
    data
  })
}

export function pageFilter(data) {
  return request({
    url: '/user/role/pageFilter',
    method: 'post',
    data
  })
}

export function list(data) {
  return request({
    url: '/user/role/list',
    method: 'post',
    data
  })
}

export function edited(data) {
  return request({
    url: '/user/role/edited',
    method: 'post',
    data
  })
}

export function deleted(data) {
  return request({
    url: '/user/role/deleted',
    method: 'post',
    data
  })
}
