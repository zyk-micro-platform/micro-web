import request from '@/utils/request'

export function list() {
  return request({
    url: '/user/menu/list',
    method: 'post'
  })
}

export function currentMenu(params) {
  return request({
    url: '/user/menu/current',
    method: 'post',
    params
  })
}

export function tree(params) {
  return request({
    url: '/user/menu/tree',
    method: 'post',
    params
  })
}

export function sync(data, params) {
  return request({
    url: '/user/menu/sync',
    method: 'post',
    data,
    params
  })
}

export function newsMenu(data) {
  return request({
    url: '/user/menu/new',
    method: 'post',
    data
  })
}

export function edited(data) {
  return request({
    url: '/user/menu/edited',
    method: 'post',
    data
  })
}

export function deleted(data) {
  return request({
    url: '/user/menu/deleted',
    method: 'post',
    data
  })
}
