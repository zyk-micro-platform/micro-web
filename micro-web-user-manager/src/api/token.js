import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/oauth/token/loginPassword',
    method: 'post',
    data: data
  })
}

export function getInfo() {
  return request({
    url: '/oauth/token/info',
    method: 'post'
  })
}

export function logout() {
  return request({
    url: '/oauth/token/logout',
    method: 'post'
  })
}
