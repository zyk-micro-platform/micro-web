/** When your routing table is too long, you can split it into small modules**/

import Layout from '@/layout'

const basicRouter = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/public/redirect/index')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/public/login/index'),
    hidden: true
  },
  {
    path: '/auth-redirect',
    component: () => import('@/public/login/auth-redirect'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/public/error-page/404'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('@/public/error-page/401'),
    hidden: true
  }
]

export default basicRouter
