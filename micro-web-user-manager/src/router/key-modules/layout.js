export default {
  'main-view': () => import('@/layout/components/MainView.vue'),
  'layout': () => import('@/layout')
}
