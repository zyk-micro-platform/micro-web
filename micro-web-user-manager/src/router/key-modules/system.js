export default {
  'function-manager': () => import('@/views/system/function-manager/index'),
  'menu-manager': () => import('@/views/system/menu-manager/index'),
  'role-manager': () => import('@/views/system/role-manager/index'),
  'user-manager': () => import('@/views/system/user-manager/index')
}
