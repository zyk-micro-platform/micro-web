import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

import basicRouter from './modules/basic'
export const constantRoutes = [...basicRouter]

const modulesFiles = require.context('./key-modules', true, /\.js$/)

export const keyRoutes = modulesFiles.keys().reduce((modules, modulePath) => {
  const value = modulesFiles(modulePath)
  modules = { ...modules, ...value.default }
  return modules
}, {})

const createRouter = () => new Router({
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher
}

router.appendRoutes = function(appendRoutes = []) {
  console.log(appendRoutes)
  this.addRoutes(appendRoutes)
  this.addRoutes([{ path: '*', redirect: '/404', hidden: true }])
}

export default router
